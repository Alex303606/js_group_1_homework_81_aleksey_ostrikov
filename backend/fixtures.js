const mongoose = require('mongoose');
const config = require('./config');
const Links = require('./models/Links');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open',async () => {
	await db.dropCollection('links');
	const link = await Links.create({
		originalUrl: 'https://www.facebook.com/',
		shortUrl: 'UUhVVcU'
	});
	console.log(link);
	db.close();
});