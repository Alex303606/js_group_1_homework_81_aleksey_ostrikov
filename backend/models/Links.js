const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LinksSchema = new Schema({
	originalUrl: {
		type: String,
		required: true
	},
	shortUrl: {
		type: String,
		required: true,
		unique: true
	}
});

const Links = mongoose.model('Links', LinksSchema);

module.exports = Links;