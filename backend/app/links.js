const express = require('express');
const Links = require('../models/Links');
const nanoid = require('nanoid');

const createRouter = () => {
	const router = express.Router();
	
	router.get('/:shortUrl', (req, res) => {
		Links.findOne({shortUrl: req.params.shortUrl})
		.then(result => {
			if (result) res.status(301).redirect(result.originalUrl);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.post('/', (req, res) => {
		const link = new Links(req.body);
		link.shortUrl = nanoid(7);
		link.save()
		.then(result => {
			res.send(result.shortUrl)
		})
		.catch(error => res.status(400).send(error));
	});
	
	return router;
};

module.exports = createRouter;