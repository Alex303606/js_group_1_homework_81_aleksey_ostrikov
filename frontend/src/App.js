import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import './App.css';
import { createLink } from "./store/actions";
import config from "./config";

class App extends Component {
	
	state = {
		originalUrl: ''
	};
	
	inputHandler = event => {
		this.setState({originalUrl: event.target.value});
	};
	
	createLink = () => {
		this.props.createLink(this.state).then(() => {
			this.setState({originalUrl: ''});
		});
	};
	
	render() {
		return (
			<div className="App">
				<h1>Shorten your link!</h1>
				<input style={this.props.error ? {borderColor: 'red', borderWidth: '2px'} : null}
				       value={this.state.originalUrl} onChange={this.inputHandler} type="text"/>
				{this.props.error && <span>Заполните поле!</span>}
				<button onClick={this.createLink}>Shorten!</button>
				{(this.props.shortLink.length !== 0 && !this.props.error) && <Fragment>
					<h2>Your link now looks like this! </h2>
					<a target="_blank"
					   href={config.apiUrl + '/' + this.props.shortLink}>
						{config.apiUrl + '/' + this.props.shortLink}</a>
				</Fragment>}
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		shortLink: state.shortLink,
		error: state.error
	};
};

const mapDispatchToProps = dispatch => {
	return {
		createLink: (link) => dispatch(createLink(link))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

