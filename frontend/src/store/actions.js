import { CREATE_LINK_SUCCESS, CREATE_ERROR_SUCCESS } from "./actionsTypes";
import axios from '../axios-api';

export const createLinkSuccess = link => {
	return {type: CREATE_LINK_SUCCESS, link};
};

export const createErrorSucces = error => {
	return {type: CREATE_ERROR_SUCCESS};
};

export const createLink = link => {
	return dispatch => {
		return axios.post('/', link).then(
			response => dispatch(createLinkSuccess(response.data))
		).catch(() => dispatch(createErrorSucces()));
	}
};