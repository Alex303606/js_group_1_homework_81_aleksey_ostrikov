import { CREATE_LINK_SUCCESS, CREATE_ERROR_SUCCESS } from "./actionsTypes";

const initialState = {
	shortLink: '',
	error: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case CREATE_LINK_SUCCESS:
			return {...state, shortLink: action.link, error: false};
		case CREATE_ERROR_SUCCESS:
			return {...state, error: true};
		default:
			return state;
	}
};

export default reducer;